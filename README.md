# LanguageComparison README

## Prerequisites

This is a simple Web application built on top of MongoDB, Express, and
AngularJS. As such, to run it you will need to install Mongo and Node.js
(including `npm` for Node.js).

## Importing Data

The initial data to populate the `langComp` database in Mongo is stored
in the `data` directory. Each JSON file corresponds to a collection
that should be created in Mongo. These files can be imported with the
following commands.

    mongoimport --db langComp2 --collection categories --jsonArray --file data\categories.json
    mongoimport --db langComp2 --collection examples --jsonArray --file data\examples.json

