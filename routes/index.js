var express = require('express');
var router = express.Router();

var mongojs = require('mongojs');

var db = mongojs('langComp');
var categories = db.collection('categories');
var examples = db.collection('examples');


/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

router.get('/categories', function(req, res) {
  categories.find().sort({order: 1}, function(err, results) {
    res.send(results);
  });
});

router.get('/category/:id', function(req, res) {
	categories.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, result) {
		res.send(result);
	});
});

router.get('/category/:id/examples', function(req, res) {
	categories.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, result) {
		examples.find({ category: result.name }).sort({order: 1}, function(err, results) {
			res.send(results);
		});
	});
});

router.get('/examples', function(req, res) {
  examples.find().sort({order: 1}, function(err, results) {
    res.send(results);
  });
});

router.get('/examples/:id', function(req, res) {
  examples.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, result) {
    res.send(result);
  });
});

module.exports = router;
