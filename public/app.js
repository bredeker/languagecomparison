var app = angular.module('langCompApp', []);

app.directive('exampleCategory', function() {
	return {
		restrict: 'E',
		templateUrl: 'example-category.html'
	};
});

app.directive('example', function() {
	return {
		restrict: 'E',
		templateUrl: 'example.html'
	};
});

app.directive('snippet', ['$log', function($log) {
	return {
		restrict: 'E',
		templateUrl: 'snippet.html'
	};
}]);

app.directive('ngPrism',['$interpolate', '$log', function ($interpolate, $log) {
	return {
		restrict: 'E',
		templateUrl: 'ng-prism.html',
		scope: {
			language: '@',
			content: '@'
		},
		replace: true,
		transclude: true,
		link: function (scope, element, attrs) {
			var codeNode = element.find('code');
			var rawText = scope.content;
			var languageDef = Prism.languages[scope.language];
			var highlightedHtml = Prism.highlight(rawText, Prism.languages[scope.language]);
			codeNode.html(highlightedHtml);
		}
	};
}]);

app.controller('LangCompCtrl', ['$scope', '$http', '$log', function($scope, $http, $log) {
	$scope.categories = [];

	$http.get('/categories').success(function(data) {
		$scope.categories = data;
	});
}])

app.controller('CategoryCtrl', ['$scope', '$http', '$log', function($scope, $http, $log) {
	$http.get('/category/' + $scope.category._id + '/examples').success(function(data) {
		$scope.category.examples = data;
	});
}]);
